[[official_docs]]
= Drupal 8 Official Docs

// Chapters are defined here, and each one includes the individual
// topic files that make up the chapter, in order.

== Evaluator Guide
include::guide_evaluator.asciidoc[]

== Getting Started Guide
include::guide_getting_started.asciidoc[]

== Configuration Management Guide
include::guide_config_management.asciidoc[]

- Composer Guide (planned)

== Appendix

[role="summary"]
Overview of copyright and contributors to these guides.

include::copyright.asciidoc[]
include::attributions.asciidoc[]
